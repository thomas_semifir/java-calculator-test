import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CalculatriceTest {

    private Calculatrice calculatrice;

    @BeforeEach
    public void setCalculatrice(){
        calculatrice = new Calculatrice();
    }

    @Test
    @DisplayName("Addition should work")
    void testAddition() {
        assertEquals(4, calculatrice.additionner(2,2), "L'addition de 2 et 2 doit donner 4");
    }

    @Test
    @DisplayName("Multiplication should work")
    void testMultiplier() {
        assertEquals(8, calculatrice.multiplier(4,4), "La multiplication de 2 et 4 doit donner 8");
    }

    @RepeatedTest(5)
    @DisplayName("Multiplication par 0 doit donner 0")
    void testMultiplierParZero(){
        for (int i = 0; i < 5; i++) {
            assertEquals(0, calculatrice.multiplier(0,i), "Une multiplication par 0 doit donner 0");
        }
    }
}
